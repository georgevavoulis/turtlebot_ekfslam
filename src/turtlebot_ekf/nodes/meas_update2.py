#!/usr/bin/env python

# Subscribed to: /odom
#				 /scan
#
# Publishes to:  /state_estimate

##########
# Import #
##########
import numpy as np
from math import *

import rospy
import sys
import tf

import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse

import std_msgs.msg
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry
from my_tutorial.msg import * 
from tf.transformations import quaternion_from_euler
from visualization_msgs.msg import Marker
import roslib#; roslib.load_manifest('ellipse_marker')
from geometry_msgs.msg import PointStamped
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import PoseWithCovarianceStamped

from geometry_msgs.msg import PoseWithCovarianceStamped, Quaternion, Pose, Point, Vector3
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header, ColorRGBA

##################
# initialization #
##################
# init 'state estimate' and 'covariance estimate'
mu_est = np.zeros((3,))
cov_est = np.array(
	[[.001, 0, 0],
	[0, .001, 0],
	[0, 0, .001]]
	)
# ... of the time
prev_time = 0

# auta pou ginontai publish ws msg typou 'Config', pws prepei na ta arxikopoihsw?????
predicted_state_est = Config(mu_est[0], mu_est[1], mu_est[2])
updated_state_est = Config(mu_est[0], mu_est[1], mu_est[2])

#########################################
# hyperparameters ---- DECIDE!!!! ----- #
#########################################
# 1. landmark extraction

# 1.1. filter to detect blobs of size 2*sigma.
sigma = 30 # landmarks (the poles) take from 60-many up to 120-many # of ranges in the 'r' list. thus, 2*sigma needs to be >60 (larger -> slower, but we need it)
d = 3*sigma # rule of size for gaussian filter
r = range(-d,d)
amp = 5 # amplitude of the LoG
filt = [-amp* ((float(x)**2 - sigma**2)/(sigma**4) * exp(-float(x)**2/(2*sigma**2))) for x in r] # detect blob with 2nd derivative gauss 
# ... or detect edges; but if EKF SLAM detects many edges -> cov matrix will explode!
#sigma = 4 # [-sigma ... sigma] should be the size of the landmark
#d = 3*sigma # rule of size for gaussian filter
#r = range(-d,d) 
#filt = [(-float(x)/(sigma**2) * exp(-float(x)**2/(2*sigma**2))) for x in r] #detect edge with 1st derivative gauss

# 1.2. ...and from them accept 75-percentile largest responses ...
thresh_percent = .9

# 1.3. ... and only the ones that give response at least .1*amp. they will be the candidate-poles.
thresh_accept = .05*amp

# 2. landmark association 
thresh_mah = .04 # .004

# 3. measurement noise Qt=[[rr, rb], [br, bb]], where r;range, b;bearing
sigma_range = .2
sigma_bearing = .02
Qt = np.diag([sigma_range, sigma_bearing])

# 4. append new landark uncertainty
sigma_append = 10.

##################
# main algorithm #
##################

# create instance of publisher
#pub1 = rospy.Publisher('state_estimate', Config, queue_size =10)
#pub1 = rospy.Publisher('state_estimate', Marker, queue_size =10)
#pub1 = rospy.Publisher('state_estimate', PointStamped, queue_size =10)
pub1 = rospy.Publisher('state_estimate', PoseWithCovarianceStamped, queue_size =10)

# create list to store all landmarks and publish markers for landmark visualization
landmarks = MarkerArray()
landmarks_cov = MarkerArray()
found_landmark = False # flag to tell when a new landmark is observed
id_no = 0	# create unique landmark identifiers
landmarks_publisher = rospy.Publisher('visualization_marker_array', MarkerArray, queue_size=5)
landmarks_cov_publisher = rospy.Publisher('visualization_marker_array2', MarkerArray, queue_size=5)

# Create publishers, subscribers, timers ... 
def get_data(): 

	# Initiate node
	rospy.init_node('meas_update', anonymous=True)

	# Initiate subscriber to /odom topic
	# /odom data publishing frequency: 100 hz
	rospy.Subscriber('/odom', Odometry, odom_state_prediction) # preciction step

	# Initiate subscriber to /scan topic. 
	# pointcloud_to_laserscan package converts kinect 3D pcl to 2D laser scan
	# /scan data publishing frequency: ~8.5-9.25 hz
	rospy.Subscriber('/scan', LaserScan, kinect_scan_estimate) # update step

	# Inintialize publisher on topic '/scan', s.t. rviz can read this topic and we can visualize it.
	pub2 = rospy.Publisher('/scan', LaserScan, queue_size=10)
	pub2.publish()
	# Did 'ros hertz' to determine how fast data is being published and subsequently recieved. 
	# Data publishing rate will determine how fast the filter can be run, 
	# Note to self: ...Or maybe it's just fine at a fixed rate?

	# Create a timed callback to the measurement update function. 
	# The callback is scheduled for every 1/1127 th of a second (8.8hz)
	#rospy.Timer(rospy.Duration(0.1127), update_step, oneshot=False)

	#rospy.Subscriber('state_estimate', Config, callback_broadcast)

	# spin() simply keeps python from exiting until this node is stopped
	rospy.spin()

	# The timer rate for now seems to ensure that the correct combination of scan data and odometer data are being sent to the
	# filter. 
	

# Step.1. use 'odom_data' (v,w,theta,R) to get 'predicted_state_est' and 'predicted_cov_est' 
def odom_state_prediction(odom_data):
	
	global mu_est
	global cov_est
	global num_land
	global predicted_state_est # i want to publish it, and draw it in rviz
	global prev_time

	####################################################
	## 1. get current data (u,v,theta,R) from odometry #
	####################################################

	# 1.1. get time of odometry to formulate 'dt'.
	time = odom_data.header.stamp.secs + odom_data.header.stamp.nsecs*1e-9

	# get yaw (theta) from the quaternion using the tf.transformations euler_from_quaternion function. 
	#(roll, pitch, true_theta) = euler_from_quaternion([odom_data.pose.pose.orientation.x, odom_data.pose.pose.orientation.y, odom_data.pose.pose.orientation.z, odom_data.pose.pose.orientation.w])

	# get x,y position of the robot
	#true_x = odom_data.pose.pose.position.x
	#true_y = odom_data.pose.pose.position.y

	# package the 'predicted state estimation' concerning the robot, in a custom message of type 'Config' 
	#predicted_state_est[0:3] = Config(true_x, true_y, true_theta)

	#rospy.loginfo(predicted_state_est)

	# 1.2. get also the linear and angular velocities
	#p = odom_data.twist.twist.angular.x # don't need 
	#q = odom_data.twist.twist.angular.y # don't need
	w = odom_data.twist.twist.angular.z # angular velocity around z-axis - add/noise or not? look sample_mortion_model_velocity

	v = odom_data.twist.twist.linear.x # straight linear velocity
	#u = odom_data.twist.twist.linear.y # don't need
	#r = odom_data.twist.twist.linear.z # don't need

	# attention!! i won't use true_x, true_y, true_theta. that would be cheating!
	# i will use the previous state estimate to predict the current state estimate..
	# that's why, in addition to velocities, (w,v), i need the previous theta.
	theta = mu_est[2]

	# 1.3. UNCERTAINTY INTRODUCED BY STATE TRANSITION (MEAN = 0, COVARIANCE PUBLISHED BY ODOM TOPIC: )
	# Odom covariance matrix is 6 x 6. We need a 3 x 3 covariance matrix of x, y and theta. Omit z, roll and pitch data.
	# this state transition uncertainty noise is called 'R'
	Rt = np.array(
	        [[odom_data.pose.covariance[0], odom_data.pose.covariance[1], odom_data.pose.covariance[5]],
	        [odom_data.pose.covariance[6], odom_data.pose.covariance[7], odom_data.pose.covariance[11]],
	        [odom_data.pose.covariance[30], odom_data.pose.covariance[31], odom_data.pose.covariance[35]]]
	)
	#print "Rt", Rt

	###################################################
	# 2. state-transition; predict state_est, cov_est #
	###################################################

	# 2.1. calculate dt ; (time i got the current odom data) - (time i had the previous odom data)          
	dt = time - prev_time
	#print "dt", dt

	# 2.2. define state transition model, and its jacobian
	# 2.2.1. state transition matrix
	if w!=0: 
		motion_model = np.array(
			[-(v/w)*sin(theta) + (v/w)*sin(theta+w*dt),
			(v/w)*cos(theta) - (v/w)*cos(theta+w*dt),
			w*dt]
		) # (3,)

	 # if w==0, driving straight - http://ros-developer.com/2019/04/11/extended-kalman-filter-explained-with-python-code/
	 # it is almost never the case. even when going straight, w is of ~10^-3
	else:
		w = .001 # avoid numerical issues in Jacobians
		motion_model = np.array(
		    [v*dt*cos(theta),
		    v*dt*sin(theta),
		    0]
		    ) # (3,) 
	
	# 2.2.2. jacobian of the state transition matrix
	motion_model_jacobian = np.array(
		[[0, 0, -(v/w)*cos(theta) + (v/w)*cos(theta+w*dt)],
		[0, 0, -(v/w)*sin(theta) + (v/w)*sin(theta+w*dt)],
		[0, 0, 0]]
		) # (3,3)
	#print "v/w",v/w,"theta", theta , "motion_model_jacobian", motion_model_jacobian

	# 2.3. prediction
	# 2.3.1. predict state estimate x(t+1) = x(t) + state_transition + noise.
	# attention ; we only change the robot position (x,y,theta), according to motion model. landmarks stay right put!
	mu_est[0] += motion_model[0]
	mu_est[1] += motion_model[1]
	mu_est[2] += motion_model[2]

	# define it so we can draw it next
	predicted_state_est = Config(mu_est[0], mu_est[1], mu_est[2])

	# 2.3.2. we will need the Fx matrix (3,3+2n)
	num_land = (np.shape(mu_est)[0] - 3) / 2
	#Fx = np.zeros((3, 3+2*num_land))
	#Fx[0:3, 0:3] = np.eye(3)

	# 2.3.3. Gt
	#Gt = np.eye(3+2*num_land) + np.dot(Fx.T, np.dot(motion_model_jacobian,Fx)) 

	# 2.3.4. predict the total covariance of the predicted state estimate
	#cov_est = (np.dot(Gt, np.dot(cov_est,Gt.T))) + (np.dot(Fx.T, np.dot(Rt,Fx))) # (3+2n,3+2n)*(3+2n,3+2n)*(3+2n,3+2n) + the same 

	# or just do 2.3.2 through 2.3.4 - it should be the same and quicker
	Gt = np.eye(3) + motion_model_jacobian # (3,3) + (3,3)
	cov_est[0:3,0:3] = (np.dot(Gt, np.dot(cov_est[0:3,0:3],Gt.T))) + Rt*dt
	#print "Gt", Gt, "cov_est", cov_est
	
	# 2.4. update prev_time to be used next time 
	prev_time = time

	
# Step.2. Get the measured range. 
# Simplest measurement model using scan data. We are throwing most of the data away for the sake of simplicity. Having more measurement data will reduce uncertainty.
# if the PCL was used instead of laser scan, the measurement update model would PERHAPS reduce the uncertainty more...  
def kinect_scan_estimate(scan_data):

	global mu_est
	global cov_est
	global num_land
	global num_cand
	global updated_state_est # Config 
	global updated_pose # PoseWithCovarianceStamped
	global id_no

	##############################
	# 1. get ranges and bearings #
	##############################

	# 1.1. ranges ; from right-most (at -pi/6) to left-most (at pi/6)
	r = np.array(scan_data.ranges) # 'ranges' ; tuple, length 640 -> 'r' ; array (640,)
	num_scans = r.shape[0] # 640
	#r_max = np.nanmax(r[r != np.inf]) # get max value, ignore Inf
	r_max = np.nanmax(r) # get r_max, ignore nan 
	r_mean = np.nanmean(r)
	inds = np.where(np.isnan(r))
	r[inds] = r_mean
	#r[inds] = r_max # interpolation #1
	for i in list(inds[0]): # interpolation #2
	    if (i-1)<0:
	        r[i] = r[i+1]
	    elif (i+1)==num_scans:
	        r[i] = r[i-1]
	    else:
	        r[i] = (r[i-1] + r[i+1])/2
	#print "r", r

	# 1.2. bearings ; from rightmost (-pi/6)  to left (pi/6)
	angle_min = scan_data.angle_min
	#angle_max = scan_data.angle_max
	angle_step = scan_data.angle_increment
	i = np.array(range(0, num_scans))
	b = angle_min + i*angle_step # array (640,) - from rightmost (-pi/6) to leftmost (pi/6)
	#b = angle_max - i * angle_step # from rightmost (pi/6) to leftmost (-pi/6)
	#print "b", b

	##########################
	# 2. landmark extraction #
	##########################

	# idea#1 (slower). extract the 'cand_land_idx' list
	# 2.1. convolve with LoG to find blobs of size 2*sigma; large resp = blob
	signal = (r_max - r)
	resp = np.convolve(signal, filt, 'same') # array type 'resp' (640,)
	#resp_mean = mean(resp)
	#resp = resp - resp_mean

	print "resp_max", max(resp)

	# 2.2. keep only peak responses, at the top 75-percentile 
	peaks_bool = np.r_[(resp[1:] < resp[:-1], True)] & np.r_[True, (resp[:-1] < resp[1:])] & (resp>thresh_percent*max(resp)) & (resp>thresh_accept)
	cand_land_idx = [i for i, x in enumerate(peaks_bool) if x]

	# idea#2. (quicker-less accurate) ; detect spikes of change (increase/decrease) in range 'r'. 
	# don't know what to do with it, just left it here for the future :) maybe combine the two ideas and  
	# use np.correlate (idea#1) ONLY in the regions, where there is indeed a spike (idea#2)...
	#spikes_val = numpy.absolute(r_measurements[0:-1] - r_measurements[1:]) # calc difference between all the r_measurements with their respective right neighbor.
	#cand_land_idx = [i for i, x in enumerate(spikes_val) if x > thresh_cand] # keep idx of r_measurements that are above threshold
	#cand_land_idx = 

	# idea#3. fast fourier convolution. but maybe it's not worth it due to small length of measurements (640)

	# 2.3. keep only the r_measurements, bearing_measurements of their respective candidate-landmarks
	# and store them compactly, in a (2, num_cand) array
	num_cand = len(cand_land_idx)
	cand_r = r[cand_land_idx]
	cand_b = b[cand_land_idx]
	z = np.concatenate((cand_r.reshape(1,num_cand), cand_b.reshape(1,num_cand)), axis=0)# (2,num_candidates)
	print "z", z

    ##############################################
    # 3. landmark association + update-or-append #
    ##############################################

    # for each candidate 'i', check if it matches any of the landmarks 'j', i have seen before. 
    # - if it does, then update mu_est, cov_est.
    # - if not, then append new landmark to the mu_est and pad cov_est.

	for i in range(num_cand): # for each observed, candidate-landmark 'i'
	
	    # 3.1. landmark association step; is landmark 'i' associated with landmark 'j'?
	    p_prev = thresh_mah # thres_mah; the higher it is, the easier it gets to match candidate-'i' to seen-'j' and update.(the harder to create new landmark)
	    land_corr = 0 # initialize indx for correlation
	    for j in range(num_land+1): # for each already-seen-landmark 'j'
	        [x,y], p, H, Psi_inv, resid = calc_dist(j, i, z[:,i])
	        #print "j",j, "p", p, "x,y", x,y
	        if p<=p_prev:
				land_corr = j # corresponding landmark index
				H_corr = H
				Psi_inv_corr = Psi_inv
				resid_corr = resid
				p_prev = p # update the running comparison
				newxy = [x,y]
				
	    # 3.2. update /or append step
	    if land_corr < num_land: # update step
		    K = np.matmul(cov_est, np.matmul(H_corr.T, Psi_inv_corr)) # (2n+3,2n+3)*(2n+3,2)*(2,2) = (2n+3,2)
		    innov = np.matmul(K, resid_corr) # (3+2n,2) * (2,1) = (3+2n,1)
		    mu_est = mu_est.reshape((innov.shape[0], 1)) + innov # (3+2n,1) + same
		    mu_est = mu_est.reshape((innov.shape[0], )) # (3+2n,1) -> (3+2n, )
		    cov_est = np.matmul( (np.eye(3+2*num_land) - np.matmul(K, H_corr)), cov_est) # (3+2n,3+2n)-(3+2n,2)*(2,3+2n)*(3+2n,3+2n)
		    print "----update w.r.t.", land_corr, mu_est.shape, cov_est.shape
	    else: # append step; if no other seen-landmark was close to candidate-landmark, then it is a new landmark!! 
		    num_land = num_land + 1
		    mu_est = np.append(mu_est, newxy) # 3+2n -> 3+2(n+1)
	        # maybe should pad with the robot (covar_xx, covar_yy) values, for numerical stability!! - p.329 thrun
	        # or maybe update the off diagonal too - look freibourg lec.13-slam.pdf - 27/49
		    cov_est = np.pad(cov_est, ((0,2),(0,2)), 'constant', constant_values=(0))
		    cov_est[-2:, -2:] = np.diag([sigma_append, sigma_append])
		    print "----found new landmark!", mu_est.shape, cov_est.shape
		    
		    found_landmark = True
		    id_no += 1
		    show_landmarks_in_rviz(landmarks_publisher, landmarks_cov_publisher,
		    					found_landmark, id_no, mu_est, cov_est)

	#print "---mu_est", mu_est, "cov_est", cov_est

	updated_state_est = Config(mu_est[0], mu_est[1], mu_est[2])

	# publish as Config 
	#updated_state_est = Config(mu_est[0], mu_est[1], mu_est[2])
	#rospy.logdebug(updated_state_est)
	#pub1.publish(updated_state_est)

	# publish as PoseWithCovarianceStamped
	pose_stamped = create_pose(updated_state_est, cov_est) # (config, np.array) -> PoseWithCovarianceStamped
	rospy.logdebug(pose_stamped)
	pub1.publish(pose_stamped)

############ I DONT USE IT #############
########################################
def callback_broadcast(config_data):
	
	br = tf.TransformBroadcaster()
	br.sendTransform((config_data.x, config_data.y, 0),
			tf.transformations.quaternion_from_euler(0, 0, config_data.th),
			rospy.Time.now(),
			odom,
			"map")
#######################################
#######################################

def create_pose(config_data, cov_data):

	pose_stamped = PoseWithCovarianceStamped()
	pose_stamped.header.frame_id = 'odom'
	pose_stamped.header.stamp = rospy.Time.now()

	pose_stamped.pose.pose.position.x = config_data.x
	pose_stamped.pose.pose.position.y = config_data.y 
	pose_stamped.pose.pose.position.z = 0

	quaternion = tf.transformations.quaternion_from_euler(0, 0, config_data.th)
	pose_stamped.pose.pose.orientation.x = quaternion[0]
	pose_stamped.pose.pose.orientation.y = quaternion[1]
	pose_stamped.pose.pose.orientation.z = quaternion[2]
	pose_stamped.pose.pose.orientation.w = quaternion[3]

	pose_stamped.pose.covariance = [
				cov_est[0,0], .1, 0.0, 0.0, 0.0, .1,
				.1, cov_est[1,1], 0.0, 0.0, 0.0, .1,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				.1, .1, 0.0, 0.0, 0.0, .1
				]


	return pose_stamped

def show_landmarks_in_rviz(landmarks_publisher, landmarks_cov_publisher,
							found_landmark, id_no, mu_est, cov_est):
	if found_landmark == True:
		landmark = Marker(header=Header(frame_id='odom'),
							ns="lndmrk",
							id=id_no,
							type=Marker.CYLINDER,
							action=Marker.ADD, 
							#pose=Pose( Point(pose_stamped.pose.pose.position.x, pose_stamped.pose.pose.position.y, pose_stamped.pose.pose.position.z),
							#	Quaternion(pose_stamped.pose.pose.orientation.y, pose_stamped.pose.pose.orientation.x,
							#				pose_stamped.pose.pose.orientation.z, pose_stamped.pose.pose.orientation.w) ),
							pose=Pose(Point(mu_est[-2], mu_est[-1], 0.0),Quaternion(0, 0, 0, 1)),
							scale=Vector3(0.4, 0.4, 0.4),
							color=ColorRGBA(1.0, 0.0, 0.0, 1.0),
							lifetime=rospy.Duration(0) )            
		
		landmarks.markers.append(landmark)
		landmarks_publisher.publish(landmarks)

		landmark_cov = Marker(header=Header(frame_id='odom'),
							ns="lndmrk_cov",
							id=id_no,
							type=Marker.SPHERE,
							action=Marker.ADD,
							pose=Pose(Point(mu_est[-2], mu_est[-1], 0.0),Quaternion(0, 0, 0, 1)),
							scale=Vector3(np.sqrt(cov_est[-3][-3]), np.sqrt(cov_est[-2][-2]), np.sqrt(cov_est[-1][-1])),
							color=ColorRGBA(0.0, 1.0, 0.0, 0.2),
							lifetime=rospy.Duration(0) )    

		
		landmarks_cov.markers.append(landmark_cov)
		landmarks_cov_publisher.publish(landmarks_cov)
		#rospy.sleep(0.5)
		id_no += 1
		found_landmark = False


def calc_dist(j,i,z):

	# calculate mahalanobis distance
    
    # 0. define the coordinates, given 'j'
    if (j==num_land): # Nt+1 ; if 'j' is the case of the current candidate landmark 'i'
    	x = np.float(mu_est[0]) + np.float(z[0])*cos(np.float(z[1])+np.float(mu_est[2])) 
    	y = np.float(mu_est[1]) + np.float(z[0])*sin(np.float(z[1])+np.float(mu_est[2]))
    else: # Nt ; if 'j' is a seen landmark
    	x = np.float(mu_est[2*j+3])
    	y = np.float(mu_est[2*j+4])
    
    # 1. delta(j)
    delta_x = x - np.float(mu_est[0])
    delta_y = y - np.float(mu_est[1])
    delta = np.array([delta_x, delta_y]) # (2,)
    
    # 2. zhat(j)
    q = np.matmul(delta.T, delta) # real
    zhat = np.array(
        [[sqrt(q)], 
        [np.arctan2(delta_y, delta_x) - mu_est[2]]]
        ) # (2,1) - no need to wrap zhat[1] angle around [-pi,pi] since arctan2 returns the output already in [-pi,pi]!
    
    # 3. we will need the Fxj 'selector-j' matrix, that is (3+2, 3+2n)
    if (j==num_land):
        Fxj = np.zeros((3+2, 3+2*num_land)) # (3+2, 3+2n)
        Fxj[0:3, 0:3] = np.eye(3)
    else:
        Fxj = np.zeros((3+2, 3+2*num_land)) # (3+2, 3+2n)
        Fxj[0:3, 0:3] = np.eye(3)
        Fxj[3:5, (3+2*j):(3+2*j+2)] = np.eye(2)
    
    # 4. H
    H_little = (1./np.float(q)) * np.array(
    	[[-sqrt(q)*delta_x, -sqrt(q)*delta_y, 0., sqrt(q)*delta_x, sqrt(q)*delta_y],
    	[delta_y, -delta_x, -1., -delta_y, delta_x]]
    	) # H_little; (2,5) 
    
    H = np.dot(H_little, Fxj) # H; (2,5)*(5,3+2n) = (2,3+2n)
    
    # 5. Psi
    Psi = np.dot(H, np.dot(cov_est, H.T)) + Qt # (2,3+2n)*(3+2n,3+2n)*(3+2n,2) + (2,2) = (2,2)
    Psi_inv = np.linalg.inv(Psi)
    
    # 6. residual error between real measurement 'z' and the expected measurement 'zhat',
    # if it was indeed a case of observing a seen landmark 'j' and not a new landmark 'i'.
    residual = z.reshape((2,1)) - zhat

    # 7. wrap the angle of residual in [-pi,pi]
    while residual[1] > np.pi:
    	residual[1] -= 2*np.pi
    while residual[1] < -np.pi:
    	residual[1] += 2*np.pi
    
    # 8. mahalobnis distance between 'i' and 'j'
    if (j==num_land): # if j == num_land, that's the Nt+1 case
        p = thresh_mah
    else: 
        p = residual.T.dot(Psi_inv).dot(residual)[0,0]

    print "j",j, "p", p, "x,y", x,y, "zhat", zhat, "mu_est", mu_est[0:3]

    # --special case - haven't figured it out yet.
    if p<0:
    	# i hardcoded this because i can't find why the fuck sometimes Psi_inv gets negatvie elements. That would mean that 
    	# Psi has negative elements, but Psi is the dot product of H*cov*H.T, where cov is semi-definite positive! i'm doing
    	# the checking below. comment out the line p=10., to see for yourself... Moreover, mahalanobis distance must be >0...
    	p = 10. 
    	print "Psi_inv", Psi_inv, "cov_est", cov_est, "residual", residual, "H_little", H_little
    	for i in range(0, cov_est.shape[0]): # check, that no on-diagonal value of covariance is <0. that would be wrong!
			if np.diag(cov_est)[i]<0:
				print "-----------------errorrrrrrr-------------"
    
    return [x, y], p, H, Psi_inv, residual


######################### I DONT USE IT ##############################
######################################################################
def draw():
# Plotting the predicted and updated state estimates as well as the uncertainty ellipse to see if 
# filter is behaving as expected. 

	fig = plt.figure(1)
	ax = fig.gca()
	plt.axis('equal')
	ax1 = plt.gca()

	# Updated state estimate: 
	x_updated = []
	y_updated = []

	plt.ion()
	plt.show()

	x_updated.append(updated_state_est.x)
	y_updated.append(updated_state_est.y)

	# Update is plotted as blue points. 
	plt.plot(x_updated,y_updated,'b*')
	plt.ylabel("y")
	plt.xlabel("x")

	# Predicted state estimate: 
	x_predict = []
	y_predict = []

	x_predict.append(predicted_state_est.x)
	y_predict.append(predicted_state_est.y)

	# Prediction is plotted as red points. 
	plt.plot(x_predict, y_predict, 'ro')
	plt.ylabel("y")
	plt.xlabel("x")


	# Plot the covariance
	# I expect the updated covariance to decrease in the direction of measurement and increase in the 
	# direction that I am not taking any measurements.  

	#lambda_pre,v=numpy.linalg.eig(pre_cov_store)
	#lambda_pre = numpy.sqrt(lambda_pre)

	#ax = plt.subplot(111, aspect = 'equal')

	#for j in xrange(1,4):
	#	ell = Ellipse(xy=(numpy.mean(x_predict),numpy.mean(y_predict)), width=lambda_pre[0]/(j*19), height=lambda_pre[1]/(j*10),angle=numpy.rad2deg(numpy.arccos(v[0,0])))

	#ell.set_facecolor('none')
	#ax.add_artist(ell)

	#lambda_up,v=numpy.linalg.eig(up_cov_store)
	#lambda_up= numpy.sqrt(lambda_up)

	#ax = plt.subplot(111, aspect = 'equal')

	#for j in xrange(1,4):
	#	ell = Ellipse(xy=(numpy.mean(x_updated),numpy.mean(y_updated)), width=lambda_up[0]/j*10, height=lambda_up[1]/j*10,angle=numpy.rad2deg(numpy.arccos(v[0,0])))
	#ell.set_facecolor('none')
	#ax.add_artist(ell)

	plt.show()
	plt.draw()
	plt.grid

#############################################
#############################################

if __name__ == '__main__':
	#When program is run, first get the measurements
	try: get_data()
	except rospy.ROSInterruptException: pass
