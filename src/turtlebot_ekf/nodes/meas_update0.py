#!/usr/bin/env python

# Subscribed to: /odom
#				 /scan
#
# Publishes to:  /state_estimate

##########
# Import #
##########
import rospy
import sys
import numpy as np
import std_msgs.msg
import matplotlib.pyplot as plt
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry
from my_tutorial.msg import * 
from tf.transformations import euler_from_quaternion
from math import *
from matplotlib.patches import Ellipse
  
import roslib
#roslib.load_manifest('learning_tf')
import rospy
import tf
from geometry_msgs.msg import PoseWithCovarianceStamped

##################
# initialization #
##################
# init 'state estimate' and 'covariance estimate'
mu_est = np.zeros((3,))
cov_est = np.array([[.1, 0, 0],
	[0, .1, 0],
	[0, 0, .1]])
# ... of the time
prev_time = 0

predicted_state_est = Config(mu_est[0], mu_est[1], mu_est[2])

#########################################
# hyperparameters ---- DECIDE!!!! ----- #
#########################################
# 1. landmark extraction
# 1.1. filter to detect blobs of size 2*sigma.
sigma = 4
d = 3*sigma # rule of size for gaussian filter
r = range(-d,d) 
filt = [-5* ((float(x)**2 - sigma**2)/(sigma**4) * exp(-float(x)**2/(2*sigma**2))) for x in r] # detect blob with 2nd derivative gauss 
# ... or detect edges; but if EKF SLAM detects many edges -> cov matrix will explode!
#sigma = 4 # [-sigma ... sigma] should be the size of the landmark
#d = 3*sigma # rule of size for gaussian filter
#r = range(-d,d) 
#filt = [(-float(x)/(sigma**2) * exp(-float(x)**2/(2*sigma**2))) for x in r] #detect edge with 1st derivative gauss
# 1.2. ...and from them accept 75-percentile largest responses. they will be the candidate-poles.
thresh_percent = .75

# 2. landmark association 
thresh_mah = .03

# 3. measurement noise (rr, rb ; br , bb), r;range, b;bearing
sigma_range = 100.
sigma_bearing = 10.
Qt = np.diag([sigma_range, sigma_bearing])

#####################
# run the algorithm #
#####################

# create instance of publisher
pub1 = rospy.Publisher('state_estimate', Config, queue_size =10)

# Create publishers, subscribers, timers ... 
def get_data(): 

	# Initiate node
	rospy.init_node('meas_update', anonymous=True)

	# Initiate subscriber to /odom topic
	# /odom data publishing frequency: 100 hz
	rospy.Subscriber('/odom', Odometry, odom_state_prediction) # preciction step

	# Initiate subscriber to /scan topic. 
	# pointcloud_to_laserscan package converts kinect 3D pcl to 2D laser scan
	# /scan data publishing frequency: ~8.5-9.25 hz
	rospy.Subscriber('/scan', LaserScan, kinect_scan_estimate) # update step

	# Inintialize publisher on topic '/scan', s.t. rviz can read this topic and we can visualize it.
	pub2 = rospy.Publisher('/scan', LaserScan, queue_size=10)
	pub2.publish()
	# Did 'ros hertz' to determine how fast data is being published and subsequently recieved. 
	# Data publishing rate will determine how fast the filter can be run, 
	# Note to self: ...Or maybe it's just fine at a fixed rate?

	# Create a timed callback to the measurement update function. 
	# The callback is scheduled for every 1/1127 th of a second (8.8hz)
	#rospy.Timer(rospy.Duration(0.1127), meas_update_step, oneshot=False)

	rospy.Subscriber('state_estimate', Config, callback_broadcast)
	
	# spin() simply keeps python from exiting until this node is stopped
	rospy.spin()

	# The timer rate for now seems to ensure that the correct combination of scan data and odometer data are being sent to the
	# filter. 

# Step.1. use 'odom_data' (v,w,theta,R) to get 'predicted_state_est' and 'predicted_cov_est' 
def odom_state_prediction(odom_data):
	
	global mu_est
	global cov_est
	global num_land
	global predicted_state_est
	global prev_time

	####################################################
	## 1. get current data (u,v,theta,R) from odometry #
	####################################################

	# 1.1. get time of odometry to formulate 'dt'.
	time = odom_data.header.stamp.secs + odom_data.header.stamp.nsecs*1e-9

	# get yaw (theta) from the quaternion using the tf.transformations euler_from_quaternion function. 
	#(roll, pitch, true_theta) = euler_from_quaternion([odom_data.pose.pose.orientation.x, odom_data.pose.pose.orientation.y, odom_data.pose.pose.orientation.z, odom_data.pose.pose.orientation.w])

	# get x,y position of the robot
	#true_x = odom_data.pose.pose.position.x
	#true_y = odom_data.pose.pose.position.y

	# package the 'predicted state estimation' concerning the robot, in a custom message of type 'Config' 
	#predicted_state_est[0:3] = Config(true_x, true_y, true_theta)

	#rospy.loginfo(predicted_state_est)

	# 1.2. get also the linear and angular velocities
	#p = odom_data.twist.twist.angular.x # don't need 
	#q = odom_data.twist.twist.angular.y # don't need
	w = odom_data.twist.twist.angular.z # angular velocity around z-axis - add/noise or not? look sample_mortion_model_velocity

	v = odom_data.twist.twist.linear.x # straight linear velocity
	#u = odom_data.twist.twist.linear.y # don't need
	#r = odom_data.twist.twist.linear.z # don't need

	# attention!! i won't use true_x, true_y, true_theta. that would be cheating!
	# i will use the previous state estimate to predict the current state estimate..
	# that's why, in addition to velocities, (w,v), i need the previous theta.
	theta = mu_est[2]

	# 1.3. UNCERTAINTY INTRODUCED BY STATE TRANSITION (MEAN = 0, COVARIANCE PUBLISHED BY ODOM TOPIC: )
	# Odom covariance matrix is 6 x 6. We need a 3 x 3 covariance matrix of x, y and theta. Omit z, roll and pitch data.
	# this state transition uncertainty noise is called 'R'
	Rt = np.array(
	        [[odom_data.pose.covariance[0], odom_data.pose.covariance[1], odom_data.pose.covariance[5]],
	        [odom_data.pose.covariance[6], odom_data.pose.covariance[7], odom_data.pose.covariance[11]],
	        [odom_data.pose.covariance[30], odom_data.pose.covariance[31], odom_data.pose.covariance[35]]]
	)

	###################################################
	# 2. state-transition; predict state_est, cov_est #
	###################################################

	# 2.1. calculate dt ; (time i got the current odom data) - (time i had the previous odom data)          
	dt = time - prev_time

	# 2.2. define state transition model, and its jacobian
	# 2.2.1. state transition matrix
	if w!=0: 
		motion_model = np.array(
			[-(v/w)*sin(theta) + (v/w)*sin(theta+w*dt),
			(v/w)*cos(theta) - (v/w)*cos(theta+w*dt),
			w*dt]
		) # (3,)

	else: # if w==0, driving straight - http://ros-developer.com/2019/04/11/extended-kalman-filter-explained-with-python-code/
		w = .0000001 # avoid numerical issues in Jacobians
		motion_model = np.array(
		    [v*dt*cos(theta),
		    v*dt*sin(theta),
		    0]
		    ) # (3,) 
	
	# 2.2.2. jacobian of the state transition matrix
	motion_model_jacobian = np.array(
		[[0, 0, -(v/w)*cos(theta) + (v/w)*cos(theta+w*dt)],
		[0, 0, -(v/w)*sin(theta) + (v/w)*sin(theta+w*dt)],
		[0, 0, 0]]
		) # (3,3)

	# 2.3. prediction
	# 2.3.1. predict state estimate x(t+1) = x(t) + state_transition + noise.
	# attention ; we only change the robot position (x,y,theta), according to motion model. landmarks stay right put!
	mu_est[0] += motion_model[0]
	mu_est[1] += motion_model[1]
	mu_est[2] += motion_model[2]

	# define it so we can draw it next
	predicted_state_est = Config(mu_est[0], mu_est[1], mu_est[2])

	# 2.3.2. we will need the Fx matrix (3,3+2n)
	num_land = (np.shape(mu_est)[0] - 3) / 2
	#Fx = np.zeros((3, 3+2*num_land))
	#Fx[0:3, 0:3] = np.eye(3)

	# 2.3.3. Gt
	#Gt = np.eye(3+2*num_land) + np.dot(Fx.T, np.dot(motion_model_jacobian,Fx)) 

	# 2.3.4. predict the total covariance of the predicted state estimate
	#cov_est = (np.dot(Gt, np.dot(cov_est,Gt.T))) + (np.dot(Fx.T, np.dot(Rt,Fx))) # (3+2n,3+2n)*(3+2n,3+2n)*(3+2n,3+2n) + the same 

	# or just do 2.3.2 through 2.3.4 - it should be the same and quicker
	Gt = np.eye(3) + motion_model_jacobian # (3,3) + (3,3)
	cov_est[0:3,0:3] = (np.dot(Gt, np.dot(cov_est[0:3,0:3],Gt.T))) + Rt

	# 2.4. update prev_time to be used next time 
	prev_time = time

	
# Step.2. Get the measured range. 
# Simplest measurement model using scan data. We are throwing most of the data away for the sake of simplicity. Having more measurement data will reduce uncertainty.
# if the PCL was used instead of laser scan, the measurement update model would PERHAPS reduce the uncertainty more...  
def kinect_scan_estimate(scan_data):

	global pub1
	global mu_est
	global cov_est
	global num_land
	global num_cand
	global updated_state_estimate

	##############################
	# 1. get ranges and bearings #
	##############################

	# 1.1. ranges 
	r = np.array(scan_data.ranges) # 'ranges' ; tuple, length 640 -> 'r' ; array (1,640)
	r_max = np.nanmax(r[r != np.inf]) # get max value, ignore Inf
	r[r>r_max] = r_max # replace Inf with r_max
	#r_max = np.nanmax(r) # r_max, ignore the nan values
	#r = [r_max if math.isnan(x) else x for x in r] # only if you keep as list; where nan, put r_max

	# 1.2. bearings 
	angle_min = scan_data.angle_min
	angle_step = scan_data.angle_increment
	num_steps = r.shape[0] # 640
	i = np.array(range(0, num_steps))
	b = angle_min + i*angle_step # array (640,)

	##########################
	# 2. landmark extraction #
	##########################

	# idea#1 (slower). extract the 'cand_land_idx' list
	# 2.1. convolve with LoG to find blobs of size 2*sigma; large resp = blob
	signal = (r_max - r)
	resp = np.convolve(signal, filt, 'same') # array type 'resp' (640,)

	# 2.2. keep only peak responses, at the top 75-percentile 
	peaks_bool = np.r_[(resp[1:] < resp[:-1], True)] & np.r_[True, (resp[:-1] < resp[1:])] & (resp>.75*max(resp))
	cand_land_idx = [i for i, x in enumerate(peaks_bool) if x]

	# idea#2. (quicker-less accurate) ; detect spikes of change (increase/decrease) in range 'r'. 
	# don't know what to do with it, just left it here for the future :) maybe combine the two ideas and  
	# use np.correlate (idea#1) ONLY in the regions, where there is indeed a spike (idea#2)...
	#spikes_val = numpy.absolute(r_measurements[0:-1] - r_measurements[1:]) # calc difference between all the r_measurements with their respective right neighbor.
	#cand_land_idx = [i for i, x in enumerate(spikes_val) if x > thresh_cand] # keep idx of r_measurements that are above threshold
	#cand_land_idx = 

	# idea#3. fast fourier convolution. but maybe it's not worth it due to small length of measurements (640)

	# 2.3. keep only the r_measurements, bearing_measurements of their respective candidate-landmarks
	# and store them compactly, in a (2, num_cand) array
	num_cand = len(cand_land_idx)
	cand_r = r[cand_land_idx]
	cand_b = b[cand_land_idx]
	z = np.concatenate((cand_r.reshape(1,num_cand), cand_b.reshape(1,num_cand)), axis=0)# (2,num_candidates)
	print "z", z

    ##############################################
    # 3. landmark association + update-or-append #
    ##############################################

    # for each candidate 'i', check if it matches any of the landmarks 'j', i have seen before. 
    # - if it does, then update mu_est, cov_est.
    # - if not, then append new landmark to the mu_est and pad cov_est.

	for i in range(num_cand): # for each observed, candidate-landmark 'i'
	
	    # 3.1. landmark association step - is landmark 'i' associated with landmark 'j'?
	    p_prev = thresh_mah # thres_mah; the higher it is, the easier it gets to match candidate-'i' to seen-'j' and update.(the harder to create new landmark)
	    land_corr = 0 # initialize indx for correlation
	    for j in range(num_land+1): # for each already-seen-landmark 'j'
	        [x,y], p, H, Psi_inv, resid = calc_dist(j, i, z[:,i])
	        print "j",j, "p", p
	        if p<=p_prev:
				land_corr = j # corresponding landmark index
				H_corr = H
				Psi_inv_corr = Psi_inv
				resid_corr = resid
				p_prev = p # update the running comparison
				newxy = [x,y]
				
	    # 3.2. update /or append step
	    if land_corr < num_land: # update step
		    K = np.matmul(cov_est, np.matmul(H_corr.T, Psi_inv_corr)) # (2n+3,2n+3)*(2n+3,2)*(2,2) = (2n+3,2)
		    innov = np.matmul(K, resid_corr) # (3+2n,2) * (2,1) = (3+2n,1)
		    mu_est = mu_est.reshape((innov.shape[0], 1)) + innov # (3+2n,1) + same
		    mu_est = mu_est.reshape((innov.shape[0], )) # (3+2n,1) -> (3+2n, )
		    cov_est = np.matmul( (np.eye(3+2*num_land) - np.matmul(K, H_corr)), cov_est) # (3+2n,3+2n)-(3+2n,2)*(2,3+2n)*(3+2n,3+2n)
		    print "update", mu_est.shape, cov_est.shape
	    else: # if no other seen-landmark was close to candidate-landmark, then it is a new landmark!! 
		    num_land = num_land + 1
		    mu_est = np.append(mu_est, newxy) # 3+2n -> 3+2(n+1)
	        # maybe should pad with the robot (covar_xx, covar_yy) values, for numerical stability!! - p.329 thrun
	        # or maybe update and the off diagonal - look freibourg lec.13-slam.pdf - 27/49
		    cov_est = np.pad(cov_est, ((0,2),(0,2)), 'constant', constant_values=(0))
		    cov_est[-2:, -2:] = np.diag([99,99])
		    print "found new landmark!", mu_est.shape, cov_est.shape

	# publish
	#updated_state_estimate = Config(mu_est[0], mu_est[1], mu_est[2])
	#rospy.logdebug(updated_state_estimate)
	#pub1.publish(updated_state_estimate)

	updated_state_estimate = create_pose() # turn Config to PoseWithCovarianceStamped
	rospy.logdebug(updated_state_estimate) 
	pub1.publish(updated_state_estimate) # publish PoseWithCovarianceStamped type msg


def callback_broadcast(config_data):
    br = tf.TransformBroadcaster()
    br.sendTransform((config_data.x, config_data.y, 0),
                     tf.transformations.quaternion_from_euler(0, 0, config_data.th),
                     rospy.Time.now(),
                     "map")

def create_pose():
    pose_stamped = PoseWithCovarianceStamped()
    pose_stamped.header.frame_id = 'odom'
    pose_stamped.header.stamp = rospy.Time.now()

    pose_stamped.pose.pose.position.x = mu_est[0]
    pose_stamped.pose.pose.position.y = mu_est[1] 
    pose_stamped.pose.pose.position.z = 0.0 

    pose_stamped.pose.pose.orientation.y = 0.0 
    pose_stamped.pose.pose.orientation.x = 0.0 
    pose_stamped.pose.pose.orientation.z = 0.0 
    pose_stamped.pose.pose.orientation.w = mu_est[2] 

    pose_stamped.pose.covariance = np.array([ [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
											    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
											    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
											    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
											    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
											    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0] ])


    return pose_stamped 

def calc_dist(j,i,z):
    
    # 0. define the coordinates, given 'j'
    if (j==num_land): # Nt+1 ; if 'j' is the case of the current candidate landmark 'i'
    	x = np.float(mu_est[0]) + np.float(z[0])*cos(np.float(z[1])+np.float(mu_est[2])) 
    	y = np.float(mu_est[1]) + np.float(z[0])*cos(np.float(z[1])+np.float(mu_est[2]))
    else: # Nk ; if 'j' is a seen landmark
    	x = np.float(mu_est[2*j+3])
    	y = np.float(mu_est[2*j+4])
    
    print "x",x
    
    # 1. delta(j)
    delta_x = x - np.float(mu_est[0])
    delta_y = y - np.float(mu_est[1])
    delta = np.array([delta_x, delta_y]) # (2,)

    print "delta_x", delta_x
    
    # 2. zhat(j)
    q = np.matmul(delta.T, delta) # real
    zhat = np.array(
        [[sqrt(q)], 
        [np.arctan2(delta_y, delta_x) - mu_est[2]]]
        ) # (2,1)
    print "q", q
    
    # 3. we will need the Fxj 'selector-j' matrix, that is (3+2, 3+2n)
    if (j==num_land):
        Fxj = np.zeros((3+2, 3+2*num_land)) # (3+2, 3+2n)
        Fxj[0:3, 0:3] = np.eye(3)
    else:
        Fxj = np.zeros((3+2, 3+2*num_land)) # (3+2, 3+2n)
        Fxj[0:3, 0:3] = np.eye(3)
        Fxj[3:5, (3+2*j):(3+2*j+2)] = np.eye(2)
    
    # 4. H
    H_little = (1/q) * np.array(
    	[[-sqrt(q)*delta_x, -sqrt(q)*delta_y, 0., sqrt(q)*delta_x, sqrt(q)*delta_y],
    	[delta_y, -delta_x, -q, -delta_y, delta_x]]
    	) # H_little; (2,5) 
    
    H = np.dot(H_little, Fxj) # H; (2,5)*(5,3+2n) = (2,3+2n)
    
    # 5. Psi
    Psi = np.dot(H, np.dot(cov_est, H.T)) + Qt # (2,3+2n)*(3+2n,3+2n)*(3+2n,2) + (2,2) = (2,2)
    print "Psi", Psi
    Psi_inv = np.linalg.inv(Psi)
    
    # 6. wrap the residual between real measurement 'z' and the expected measurement 'zhat', 
    # if this was indeed a case of observing a seen landmark 'j' and not a new landmark 'i'.
    residual = z.reshape((2,1)) - zhat
    while residual[1] > np.pi:
        residual[1] -= 2*np.pi
    while residual[1] < -np.pi:
        residual[1] += 2*np.pi
    
    # 7. mahalobnis distance between 'i' and 'j'
    if (j==num_land): # if j == num_land, that's the Nt+1 case
        p = thresh_mah
    else: 
        p = residual.T.dot(Psi_inv).dot(residual)[0,0]
    
    return [x, y], p, H, Psi_inv, residual

def draw():
# Plotting the predicted and updated state estimates as well as the uncertainty ellipse to see if 
# filter is behaving as expected. 

	fig = plt.figure(1)
	ax = fig.gca()
	plt.axis('equal')
	ax1 = plt.gca()

	# Updated state estimate: 
	x_updated = []
	y_updated = []

	plt.ion()
	plt.show()

	x_updated.append(updated_state_estimate.x)
	y_updated.append(updated_state_estimate.y)

	# Update is plotted as blue points. 
	plt.plot(x_updated,y_updated,'b*')
	plt.ylabel("y")
	plt.xlabel("x")

	# Predicted state estimate: 
	x_predict = []
	y_predict = []

	x_predict.append(predicted_state_est.x)
	y_predict.append(predicted_state_est.y)

	# Prediction is plotted as red points. 
	plt.plot(x_predict, y_predict, 'ro')
	plt.ylabel("y")
	plt.xlabel("x")


	# Plot the covariance
	# I expect the updated covariance to decrease in the direction of measurement and increase in the 
	# direction that I am not taking any measurements.  

	#lambda_pre,v=numpy.linalg.eig(pre_cov_store)
	#lambda_pre = numpy.sqrt(lambda_pre)

	#ax = plt.subplot(111, aspect = 'equal')

	#for j in xrange(1,4):
	#	ell = Ellipse(xy=(numpy.mean(x_predict),numpy.mean(y_predict)), width=lambda_pre[0]/(j*19), height=lambda_pre[1]/(j*10),angle=numpy.rad2deg(numpy.arccos(v[0,0])))

	#ell.set_facecolor('none')
	#ax.add_artist(ell)

	#lambda_up,v=numpy.linalg.eig(up_cov_store)
	#lambda_up= numpy.sqrt(lambda_up)

	#ax = plt.subplot(111, aspect = 'equal')

	#for j in xrange(1,4):
	#	ell = Ellipse(xy=(numpy.mean(x_updated),numpy.mean(y_updated)), width=lambda_up[0]/j*10, height=lambda_up[1]/j*10,angle=numpy.rad2deg(numpy.arccos(v[0,0])))
	#ell.set_facecolor('none')
	#ax.add_artist(ell)

	plt.show()
	plt.draw()
	plt.grid

if __name__ == '__main__':
	#When program is run, first get the measurements
	try: get_data()
	except rospy.ROSInterruptException: pass