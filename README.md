First of all, special thanks to MahdiehNejati for her tutorial (https://github.com/MahdiehNejati/turtlebot_ekf). We built based on her ROS-structure. 

Our implementation (George Vavoulis and Sotiris Papadopoulos) is an ekf_slam for ROS, that only uses laserscan, for landamark extraction, and this was our project for the course "Autonomous Robotics" during our master "Brain and Mind", in UoC.

Now clone the package, put it in your home/(usr) directory, where (usr) is the name of your disk and then ...

1. open terminal

1.1. on first tab: 
1.1.1. $cd turtlebot_ekfslam
1.1.2. $catkin_make
1.1.3. $source devel/setup.bash

1.2. open another tab (second tab): $roscore

1.3. open anothter tab (third tab): $roslaunch turtlebot_teleop keyboard_teleop.launch

1.4. on the first tab: 
$rospack list 

that is to check if your package is built correctly. search for 'my_tutorial' in the list. the name of the package is 'my_tutorial'.

1.5. then on the first tab do:
$roslaunch my_tutorial trtb_ekf_slam.launch

-if it raises error, then specify the complete directory for the launch file:
$roslaunch roslaunch /home/(usr)/turtlebot_ekfslam/src/turtlebot_ekf/launch/trtb_ekf_slam.launch

, where (usr) is the name of your disk... 

-if it doesn't find any node meas_update.py try: chmod +x meas_update.py

1.6. click on the tab where you opened the keyboard (third tab), to move around : 
'u' rotate clockwise, 'o' rotate anticlockwise, 'i' move straight, 'w' to accelarate..

notes on how to move around: 

- try with 'w' to move linear velocity up to .4 - .5 
- when pushing buttons to move the robot, you should click with your mouse on the tab of the terminal, that teleop runs!
- plan you route to move anticlockwise around the wall, while searching for the landmarks (i.e. blobs that take around 60-many range measurments).

comments on how it operates : 

you can well understand, that the distance from which the robot sees a pole, affects the robots ability to recognize one (i.e. if it sees 
it from far then the pole might take 60-many range values , if it sees the pole from a closer distance then the pole might take up to 500-many
range measurements...) 

i only use laserscan for extraction, so there is no way to avoid 'seeing' some other things as well, apart from poles, as landmarks.

examples: 

- a corner is a '60-many-ranges' blob
- a wall, that a see from a close distance when my visual field is almost parallel to it, and the wall is on the one side, this is a landmark too...

it doesnt do a great job, however it does a decent job, for smn who wants to show how ekf_slam works and make some points...

future work, things that need checking and correction : 
- some (p<0), that shouldn't be there (see lines 517-526).
- landmark extraction 
