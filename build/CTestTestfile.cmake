# CMake generated Testfile for 
# Source directory: /home/george/turtlebot_ekf_ws/src
# Build directory: /home/george/turtlebot_ekf_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(turtlebot_ekf)
