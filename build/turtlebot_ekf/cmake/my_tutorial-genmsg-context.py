# generated from genmsg/cmake/pkg-genmsg.context.in

messages_str = "/home/george/turtlebot_ekf_ws/src/turtlebot_ekf/msg/Config.msg;/home/george/turtlebot_ekf_ws/src/turtlebot_ekf/msg/Control.msg"
services_str = "/home/george/turtlebot_ekf_ws/src/turtlebot_ekf/srv/RefState.srv;/home/george/turtlebot_ekf_ws/src/turtlebot_ekf/srv/DesiredState.srv;/home/george/turtlebot_ekf_ws/src/turtlebot_ekf/srv/Add.srv;/home/george/turtlebot_ekf_ws/src/turtlebot_ekf/srv/Sub.srv"
pkg_name = "my_tutorial"
dependencies_str = "std_msgs"
langs = "gencpp;geneus;genlisp;gennodejs;genpy"
dep_include_paths_str = "my_tutorial;/home/george/turtlebot_ekf_ws/src/turtlebot_ekf/msg;std_msgs;/opt/ros/kinetic/share/std_msgs/cmake/../msg"
PYTHON_EXECUTABLE = "/usr/bin/python"
package_has_static_sources = '' == 'TRUE'
genmsg_check_deps_script = "/opt/ros/kinetic/share/genmsg/cmake/../../../lib/genmsg/genmsg_check_deps.py"
