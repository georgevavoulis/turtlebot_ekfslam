
"use strict";

let Config = require('./Config.js');
let Control = require('./Control.js');

module.exports = {
  Config: Config,
  Control: Control,
};
