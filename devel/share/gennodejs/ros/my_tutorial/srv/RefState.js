// Auto-generated. Do not edit!

// (in-package my_tutorial.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

let Config = require('../msg/Config.js');

//-----------------------------------------------------------

class RefStateRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.t = null;
    }
    else {
      if (initObj.hasOwnProperty('t')) {
        this.t = initObj.t
      }
      else {
        this.t = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type RefStateRequest
    // Serialize message field [t]
    bufferOffset = _serializer.float64(obj.t, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type RefStateRequest
    let len;
    let data = new RefStateRequest(null);
    // Deserialize message field [t]
    data.t = _deserializer.float64(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 8;
  }

  static datatype() {
    // Returns string type for a service object
    return 'my_tutorial/RefStateRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'fcd01ddee253f9d54db9fbbcbe1701d4';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float64 t
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new RefStateRequest(null);
    if (msg.t !== undefined) {
      resolved.t = msg.t;
    }
    else {
      resolved.t = 0.0
    }

    return resolved;
    }
};

class RefStateResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.q = null;
    }
    else {
      if (initObj.hasOwnProperty('q')) {
        this.q = initObj.q
      }
      else {
        this.q = new Config();
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type RefStateResponse
    // Serialize message field [q]
    bufferOffset = Config.serialize(obj.q, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type RefStateResponse
    let len;
    let data = new RefStateResponse(null);
    // Deserialize message field [q]
    data.q = Config.deserialize(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 24;
  }

  static datatype() {
    // Returns string type for a service object
    return 'my_tutorial/RefStateResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '5bf8c8b49b4952921bca0f23dd7ea826';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Config q
    
    
    ================================================================================
    MSG: my_tutorial/Config
    float64 x
    float64 y
    float64 th
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new RefStateResponse(null);
    if (msg.q !== undefined) {
      resolved.q = Config.Resolve(msg.q)
    }
    else {
      resolved.q = new Config()
    }

    return resolved;
    }
};

module.exports = {
  Request: RefStateRequest,
  Response: RefStateResponse,
  md5sum() { return '64bad652c7c363a730b300b0ba4db9e5'; },
  datatype() { return 'my_tutorial/RefState'; }
};
