
(cl:in-package :asdf)

(defsystem "my_tutorial-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :my_tutorial-msg
)
  :components ((:file "_package")
    (:file "Add" :depends-on ("_package_Add"))
    (:file "_package_Add" :depends-on ("_package"))
    (:file "DesiredState" :depends-on ("_package_DesiredState"))
    (:file "_package_DesiredState" :depends-on ("_package"))
    (:file "RefState" :depends-on ("_package_RefState"))
    (:file "_package_RefState" :depends-on ("_package"))
    (:file "Sub" :depends-on ("_package_Sub"))
    (:file "_package_Sub" :depends-on ("_package"))
  ))