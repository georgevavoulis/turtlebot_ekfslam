; Auto-generated. Do not edit!


(cl:in-package my_tutorial-srv)


;//! \htmlinclude RefState-request.msg.html

(cl:defclass <RefState-request> (roslisp-msg-protocol:ros-message)
  ((t
    :reader t
    :initarg :t
    :type cl:float
    :initform 0.0))
)

(cl:defclass RefState-request (<RefState-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <RefState-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'RefState-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name my_tutorial-srv:<RefState-request> is deprecated: use my_tutorial-srv:RefState-request instead.")))

(cl:ensure-generic-function 't-val :lambda-list '(m))
(cl:defmethod t-val ((m <RefState-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader my_tutorial-srv:t-val is deprecated.  Use my_tutorial-srv:t instead.")
  (t m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <RefState-request>) ostream)
  "Serializes a message object of type '<RefState-request>"
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 't))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <RefState-request>) istream)
  "Deserializes a message object of type '<RefState-request>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 't) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<RefState-request>)))
  "Returns string type for a service object of type '<RefState-request>"
  "my_tutorial/RefStateRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'RefState-request)))
  "Returns string type for a service object of type 'RefState-request"
  "my_tutorial/RefStateRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<RefState-request>)))
  "Returns md5sum for a message object of type '<RefState-request>"
  "64bad652c7c363a730b300b0ba4db9e5")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'RefState-request)))
  "Returns md5sum for a message object of type 'RefState-request"
  "64bad652c7c363a730b300b0ba4db9e5")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<RefState-request>)))
  "Returns full string definition for message of type '<RefState-request>"
  (cl:format cl:nil "float64 t~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'RefState-request)))
  "Returns full string definition for message of type 'RefState-request"
  (cl:format cl:nil "float64 t~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <RefState-request>))
  (cl:+ 0
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <RefState-request>))
  "Converts a ROS message object to a list"
  (cl:list 'RefState-request
    (cl:cons ':t (t msg))
))
;//! \htmlinclude RefState-response.msg.html

(cl:defclass <RefState-response> (roslisp-msg-protocol:ros-message)
  ((q
    :reader q
    :initarg :q
    :type my_tutorial-msg:Config
    :initform (cl:make-instance 'my_tutorial-msg:Config)))
)

(cl:defclass RefState-response (<RefState-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <RefState-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'RefState-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name my_tutorial-srv:<RefState-response> is deprecated: use my_tutorial-srv:RefState-response instead.")))

(cl:ensure-generic-function 'q-val :lambda-list '(m))
(cl:defmethod q-val ((m <RefState-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader my_tutorial-srv:q-val is deprecated.  Use my_tutorial-srv:q instead.")
  (q m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <RefState-response>) ostream)
  "Serializes a message object of type '<RefState-response>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'q) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <RefState-response>) istream)
  "Deserializes a message object of type '<RefState-response>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'q) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<RefState-response>)))
  "Returns string type for a service object of type '<RefState-response>"
  "my_tutorial/RefStateResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'RefState-response)))
  "Returns string type for a service object of type 'RefState-response"
  "my_tutorial/RefStateResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<RefState-response>)))
  "Returns md5sum for a message object of type '<RefState-response>"
  "64bad652c7c363a730b300b0ba4db9e5")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'RefState-response)))
  "Returns md5sum for a message object of type 'RefState-response"
  "64bad652c7c363a730b300b0ba4db9e5")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<RefState-response>)))
  "Returns full string definition for message of type '<RefState-response>"
  (cl:format cl:nil "Config q~%~%~%================================================================================~%MSG: my_tutorial/Config~%float64 x~%float64 y~%float64 th~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'RefState-response)))
  "Returns full string definition for message of type 'RefState-response"
  (cl:format cl:nil "Config q~%~%~%================================================================================~%MSG: my_tutorial/Config~%float64 x~%float64 y~%float64 th~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <RefState-response>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'q))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <RefState-response>))
  "Converts a ROS message object to a list"
  (cl:list 'RefState-response
    (cl:cons ':q (q msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'RefState)))
  'RefState-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'RefState)))
  'RefState-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'RefState)))
  "Returns string type for a service object of type '<RefState>"
  "my_tutorial/RefState")