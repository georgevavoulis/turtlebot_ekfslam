; Auto-generated. Do not edit!


(cl:in-package my_tutorial-srv)


;//! \htmlinclude DesiredState-request.msg.html

(cl:defclass <DesiredState-request> (roslisp-msg-protocol:ros-message)
  ((t
    :reader t
    :initarg :t
    :type cl:float
    :initform 0.0))
)

(cl:defclass DesiredState-request (<DesiredState-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <DesiredState-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'DesiredState-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name my_tutorial-srv:<DesiredState-request> is deprecated: use my_tutorial-srv:DesiredState-request instead.")))

(cl:ensure-generic-function 't-val :lambda-list '(m))
(cl:defmethod t-val ((m <DesiredState-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader my_tutorial-srv:t-val is deprecated.  Use my_tutorial-srv:t instead.")
  (t m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <DesiredState-request>) ostream)
  "Serializes a message object of type '<DesiredState-request>"
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 't))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <DesiredState-request>) istream)
  "Deserializes a message object of type '<DesiredState-request>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 't) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<DesiredState-request>)))
  "Returns string type for a service object of type '<DesiredState-request>"
  "my_tutorial/DesiredStateRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'DesiredState-request)))
  "Returns string type for a service object of type 'DesiredState-request"
  "my_tutorial/DesiredStateRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<DesiredState-request>)))
  "Returns md5sum for a message object of type '<DesiredState-request>"
  "64bad652c7c363a730b300b0ba4db9e5")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'DesiredState-request)))
  "Returns md5sum for a message object of type 'DesiredState-request"
  "64bad652c7c363a730b300b0ba4db9e5")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<DesiredState-request>)))
  "Returns full string definition for message of type '<DesiredState-request>"
  (cl:format cl:nil "float64 t~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'DesiredState-request)))
  "Returns full string definition for message of type 'DesiredState-request"
  (cl:format cl:nil "float64 t~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <DesiredState-request>))
  (cl:+ 0
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <DesiredState-request>))
  "Converts a ROS message object to a list"
  (cl:list 'DesiredState-request
    (cl:cons ':t (t msg))
))
;//! \htmlinclude DesiredState-response.msg.html

(cl:defclass <DesiredState-response> (roslisp-msg-protocol:ros-message)
  ((q
    :reader q
    :initarg :q
    :type my_tutorial-msg:Config
    :initform (cl:make-instance 'my_tutorial-msg:Config)))
)

(cl:defclass DesiredState-response (<DesiredState-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <DesiredState-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'DesiredState-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name my_tutorial-srv:<DesiredState-response> is deprecated: use my_tutorial-srv:DesiredState-response instead.")))

(cl:ensure-generic-function 'q-val :lambda-list '(m))
(cl:defmethod q-val ((m <DesiredState-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader my_tutorial-srv:q-val is deprecated.  Use my_tutorial-srv:q instead.")
  (q m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <DesiredState-response>) ostream)
  "Serializes a message object of type '<DesiredState-response>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'q) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <DesiredState-response>) istream)
  "Deserializes a message object of type '<DesiredState-response>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'q) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<DesiredState-response>)))
  "Returns string type for a service object of type '<DesiredState-response>"
  "my_tutorial/DesiredStateResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'DesiredState-response)))
  "Returns string type for a service object of type 'DesiredState-response"
  "my_tutorial/DesiredStateResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<DesiredState-response>)))
  "Returns md5sum for a message object of type '<DesiredState-response>"
  "64bad652c7c363a730b300b0ba4db9e5")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'DesiredState-response)))
  "Returns md5sum for a message object of type 'DesiredState-response"
  "64bad652c7c363a730b300b0ba4db9e5")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<DesiredState-response>)))
  "Returns full string definition for message of type '<DesiredState-response>"
  (cl:format cl:nil "Config q~%~%~%================================================================================~%MSG: my_tutorial/Config~%float64 x~%float64 y~%float64 th~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'DesiredState-response)))
  "Returns full string definition for message of type 'DesiredState-response"
  (cl:format cl:nil "Config q~%~%~%================================================================================~%MSG: my_tutorial/Config~%float64 x~%float64 y~%float64 th~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <DesiredState-response>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'q))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <DesiredState-response>))
  "Converts a ROS message object to a list"
  (cl:list 'DesiredState-response
    (cl:cons ':q (q msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'DesiredState)))
  'DesiredState-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'DesiredState)))
  'DesiredState-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'DesiredState)))
  "Returns string type for a service object of type '<DesiredState>"
  "my_tutorial/DesiredState")