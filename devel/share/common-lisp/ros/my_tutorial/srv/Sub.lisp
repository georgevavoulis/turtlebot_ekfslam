; Auto-generated. Do not edit!


(cl:in-package my_tutorial-srv)


;//! \htmlinclude Sub-request.msg.html

(cl:defclass <Sub-request> (roslisp-msg-protocol:ros-message)
  ((sum
    :reader sum
    :initarg :sum
    :type cl:float
    :initform 0.0))
)

(cl:defclass Sub-request (<Sub-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Sub-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Sub-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name my_tutorial-srv:<Sub-request> is deprecated: use my_tutorial-srv:Sub-request instead.")))

(cl:ensure-generic-function 'sum-val :lambda-list '(m))
(cl:defmethod sum-val ((m <Sub-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader my_tutorial-srv:sum-val is deprecated.  Use my_tutorial-srv:sum instead.")
  (sum m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Sub-request>) ostream)
  "Serializes a message object of type '<Sub-request>"
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'sum))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Sub-request>) istream)
  "Deserializes a message object of type '<Sub-request>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'sum) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Sub-request>)))
  "Returns string type for a service object of type '<Sub-request>"
  "my_tutorial/SubRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Sub-request)))
  "Returns string type for a service object of type 'Sub-request"
  "my_tutorial/SubRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Sub-request>)))
  "Returns md5sum for a message object of type '<Sub-request>"
  "4d5a10e57b9a637e659d8a36ed8ee7e5")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Sub-request)))
  "Returns md5sum for a message object of type 'Sub-request"
  "4d5a10e57b9a637e659d8a36ed8ee7e5")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Sub-request>)))
  "Returns full string definition for message of type '<Sub-request>"
  (cl:format cl:nil "float64 sum~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Sub-request)))
  "Returns full string definition for message of type 'Sub-request"
  (cl:format cl:nil "float64 sum~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Sub-request>))
  (cl:+ 0
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Sub-request>))
  "Converts a ROS message object to a list"
  (cl:list 'Sub-request
    (cl:cons ':sum (sum msg))
))
;//! \htmlinclude Sub-response.msg.html

(cl:defclass <Sub-response> (roslisp-msg-protocol:ros-message)
  ((a
    :reader a
    :initarg :a
    :type cl:float
    :initform 0.0)
   (b
    :reader b
    :initarg :b
    :type cl:float
    :initform 0.0))
)

(cl:defclass Sub-response (<Sub-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Sub-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Sub-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name my_tutorial-srv:<Sub-response> is deprecated: use my_tutorial-srv:Sub-response instead.")))

(cl:ensure-generic-function 'a-val :lambda-list '(m))
(cl:defmethod a-val ((m <Sub-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader my_tutorial-srv:a-val is deprecated.  Use my_tutorial-srv:a instead.")
  (a m))

(cl:ensure-generic-function 'b-val :lambda-list '(m))
(cl:defmethod b-val ((m <Sub-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader my_tutorial-srv:b-val is deprecated.  Use my_tutorial-srv:b instead.")
  (b m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Sub-response>) ostream)
  "Serializes a message object of type '<Sub-response>"
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'a))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'b))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Sub-response>) istream)
  "Deserializes a message object of type '<Sub-response>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'a) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'b) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Sub-response>)))
  "Returns string type for a service object of type '<Sub-response>"
  "my_tutorial/SubResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Sub-response)))
  "Returns string type for a service object of type 'Sub-response"
  "my_tutorial/SubResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Sub-response>)))
  "Returns md5sum for a message object of type '<Sub-response>"
  "4d5a10e57b9a637e659d8a36ed8ee7e5")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Sub-response)))
  "Returns md5sum for a message object of type 'Sub-response"
  "4d5a10e57b9a637e659d8a36ed8ee7e5")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Sub-response>)))
  "Returns full string definition for message of type '<Sub-response>"
  (cl:format cl:nil "float64 a~%float64 b~%~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Sub-response)))
  "Returns full string definition for message of type 'Sub-response"
  (cl:format cl:nil "float64 a~%float64 b~%~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Sub-response>))
  (cl:+ 0
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Sub-response>))
  "Converts a ROS message object to a list"
  (cl:list 'Sub-response
    (cl:cons ':a (a msg))
    (cl:cons ':b (b msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'Sub)))
  'Sub-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'Sub)))
  'Sub-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Sub)))
  "Returns string type for a service object of type '<Sub>"
  "my_tutorial/Sub")