;; Auto-generated. Do not edit!


(when (boundp 'my_tutorial::Sub)
  (if (not (find-package "MY_TUTORIAL"))
    (make-package "MY_TUTORIAL"))
  (shadow 'Sub (find-package "MY_TUTORIAL")))
(unless (find-package "MY_TUTORIAL::SUB")
  (make-package "MY_TUTORIAL::SUB"))
(unless (find-package "MY_TUTORIAL::SUBREQUEST")
  (make-package "MY_TUTORIAL::SUBREQUEST"))
(unless (find-package "MY_TUTORIAL::SUBRESPONSE")
  (make-package "MY_TUTORIAL::SUBRESPONSE"))

(in-package "ROS")





(defclass my_tutorial::SubRequest
  :super ros::object
  :slots (_sum ))

(defmethod my_tutorial::SubRequest
  (:init
   (&key
    ((:sum __sum) 0.0)
    )
   (send-super :init)
   (setq _sum (float __sum))
   self)
  (:sum
   (&optional __sum)
   (if __sum (setq _sum __sum)) _sum)
  (:serialization-length
   ()
   (+
    ;; float64 _sum
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _sum
       (sys::poke _sum (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _sum
     (setq _sum (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(defclass my_tutorial::SubResponse
  :super ros::object
  :slots (_a _b ))

(defmethod my_tutorial::SubResponse
  (:init
   (&key
    ((:a __a) 0.0)
    ((:b __b) 0.0)
    )
   (send-super :init)
   (setq _a (float __a))
   (setq _b (float __b))
   self)
  (:a
   (&optional __a)
   (if __a (setq _a __a)) _a)
  (:b
   (&optional __b)
   (if __b (setq _b __b)) _b)
  (:serialization-length
   ()
   (+
    ;; float64 _a
    8
    ;; float64 _b
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _a
       (sys::poke _a (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _b
       (sys::poke _b (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _a
     (setq _a (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _b
     (setq _b (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(defclass my_tutorial::Sub
  :super ros::object
  :slots ())

(setf (get my_tutorial::Sub :md5sum-) "4d5a10e57b9a637e659d8a36ed8ee7e5")
(setf (get my_tutorial::Sub :datatype-) "my_tutorial/Sub")
(setf (get my_tutorial::Sub :request) my_tutorial::SubRequest)
(setf (get my_tutorial::Sub :response) my_tutorial::SubResponse)

(defmethod my_tutorial::SubRequest
  (:response () (instance my_tutorial::SubResponse :init)))

(setf (get my_tutorial::SubRequest :md5sum-) "4d5a10e57b9a637e659d8a36ed8ee7e5")
(setf (get my_tutorial::SubRequest :datatype-) "my_tutorial/SubRequest")
(setf (get my_tutorial::SubRequest :definition-)
      "float64 sum
---
float64 a
float64 b



")

(setf (get my_tutorial::SubResponse :md5sum-) "4d5a10e57b9a637e659d8a36ed8ee7e5")
(setf (get my_tutorial::SubResponse :datatype-) "my_tutorial/SubResponse")
(setf (get my_tutorial::SubResponse :definition-)
      "float64 sum
---
float64 a
float64 b



")



(provide :my_tutorial/Sub "4d5a10e57b9a637e659d8a36ed8ee7e5")


