;; Auto-generated. Do not edit!


(when (boundp 'my_tutorial::DesiredState)
  (if (not (find-package "MY_TUTORIAL"))
    (make-package "MY_TUTORIAL"))
  (shadow 'DesiredState (find-package "MY_TUTORIAL")))
(unless (find-package "MY_TUTORIAL::DESIREDSTATE")
  (make-package "MY_TUTORIAL::DESIREDSTATE"))
(unless (find-package "MY_TUTORIAL::DESIREDSTATEREQUEST")
  (make-package "MY_TUTORIAL::DESIREDSTATEREQUEST"))
(unless (find-package "MY_TUTORIAL::DESIREDSTATERESPONSE")
  (make-package "MY_TUTORIAL::DESIREDSTATERESPONSE"))

(in-package "ROS")





(defclass my_tutorial::DesiredStateRequest
  :super ros::object
  :slots (_t ))

(defmethod my_tutorial::DesiredStateRequest
  (:init
   (&key
    ((:t __t) 0.0)
    )
   (send-super :init)
   (setq _t (float __t))
   self)
  (:t
   (&optional __t)
   (if __t (setq _t __t)) _t)
  (:serialization-length
   ()
   (+
    ;; float64 _t
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _t
       (sys::poke _t (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _t
     (setq _t (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(defclass my_tutorial::DesiredStateResponse
  :super ros::object
  :slots (_q ))

(defmethod my_tutorial::DesiredStateResponse
  (:init
   (&key
    ((:q __q) (instance my_tutorial::Config :init))
    )
   (send-super :init)
   (setq _q __q)
   self)
  (:q
   (&rest __q)
   (if (keywordp (car __q))
       (send* _q __q)
     (progn
       (if __q (setq _q (car __q)))
       _q)))
  (:serialization-length
   ()
   (+
    ;; my_tutorial/Config _q
    (send _q :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; my_tutorial/Config _q
       (send _q :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; my_tutorial/Config _q
     (send _q :deserialize buf ptr-) (incf ptr- (send _q :serialization-length))
   ;;
   self)
  )

(defclass my_tutorial::DesiredState
  :super ros::object
  :slots ())

(setf (get my_tutorial::DesiredState :md5sum-) "64bad652c7c363a730b300b0ba4db9e5")
(setf (get my_tutorial::DesiredState :datatype-) "my_tutorial/DesiredState")
(setf (get my_tutorial::DesiredState :request) my_tutorial::DesiredStateRequest)
(setf (get my_tutorial::DesiredState :response) my_tutorial::DesiredStateResponse)

(defmethod my_tutorial::DesiredStateRequest
  (:response () (instance my_tutorial::DesiredStateResponse :init)))

(setf (get my_tutorial::DesiredStateRequest :md5sum-) "64bad652c7c363a730b300b0ba4db9e5")
(setf (get my_tutorial::DesiredStateRequest :datatype-) "my_tutorial/DesiredStateRequest")
(setf (get my_tutorial::DesiredStateRequest :definition-)
      "float64 t
---
Config q


================================================================================
MSG: my_tutorial/Config
float64 x
float64 y
float64 th

")

(setf (get my_tutorial::DesiredStateResponse :md5sum-) "64bad652c7c363a730b300b0ba4db9e5")
(setf (get my_tutorial::DesiredStateResponse :datatype-) "my_tutorial/DesiredStateResponse")
(setf (get my_tutorial::DesiredStateResponse :definition-)
      "float64 t
---
Config q


================================================================================
MSG: my_tutorial/Config
float64 x
float64 y
float64 th

")



(provide :my_tutorial/DesiredState "64bad652c7c363a730b300b0ba4db9e5")


