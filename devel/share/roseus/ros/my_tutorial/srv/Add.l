;; Auto-generated. Do not edit!


(when (boundp 'my_tutorial::Add)
  (if (not (find-package "MY_TUTORIAL"))
    (make-package "MY_TUTORIAL"))
  (shadow 'Add (find-package "MY_TUTORIAL")))
(unless (find-package "MY_TUTORIAL::ADD")
  (make-package "MY_TUTORIAL::ADD"))
(unless (find-package "MY_TUTORIAL::ADDREQUEST")
  (make-package "MY_TUTORIAL::ADDREQUEST"))
(unless (find-package "MY_TUTORIAL::ADDRESPONSE")
  (make-package "MY_TUTORIAL::ADDRESPONSE"))

(in-package "ROS")





(defclass my_tutorial::AddRequest
  :super ros::object
  :slots (_b _a ))

(defmethod my_tutorial::AddRequest
  (:init
   (&key
    ((:b __b) 0.0)
    ((:a __a) 0.0)
    )
   (send-super :init)
   (setq _b (float __b))
   (setq _a (float __a))
   self)
  (:b
   (&optional __b)
   (if __b (setq _b __b)) _b)
  (:a
   (&optional __a)
   (if __a (setq _a __a)) _a)
  (:serialization-length
   ()
   (+
    ;; float64 _b
    8
    ;; float64 _a
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _b
       (sys::poke _b (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _a
       (sys::poke _a (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _b
     (setq _b (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _a
     (setq _a (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(defclass my_tutorial::AddResponse
  :super ros::object
  :slots (_sum ))

(defmethod my_tutorial::AddResponse
  (:init
   (&key
    ((:sum __sum) 0.0)
    )
   (send-super :init)
   (setq _sum (float __sum))
   self)
  (:sum
   (&optional __sum)
   (if __sum (setq _sum __sum)) _sum)
  (:serialization-length
   ()
   (+
    ;; float64 _sum
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _sum
       (sys::poke _sum (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _sum
     (setq _sum (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(defclass my_tutorial::Add
  :super ros::object
  :slots ())

(setf (get my_tutorial::Add :md5sum-) "65e00b0ea70b5737f440c3d63afd8c59")
(setf (get my_tutorial::Add :datatype-) "my_tutorial/Add")
(setf (get my_tutorial::Add :request) my_tutorial::AddRequest)
(setf (get my_tutorial::Add :response) my_tutorial::AddResponse)

(defmethod my_tutorial::AddRequest
  (:response () (instance my_tutorial::AddResponse :init)))

(setf (get my_tutorial::AddRequest :md5sum-) "65e00b0ea70b5737f440c3d63afd8c59")
(setf (get my_tutorial::AddRequest :datatype-) "my_tutorial/AddRequest")
(setf (get my_tutorial::AddRequest :definition-)
      "float64 b
float64 a
---
float64 sum


")

(setf (get my_tutorial::AddResponse :md5sum-) "65e00b0ea70b5737f440c3d63afd8c59")
(setf (get my_tutorial::AddResponse :datatype-) "my_tutorial/AddResponse")
(setf (get my_tutorial::AddResponse :definition-)
      "float64 b
float64 a
---
float64 sum


")



(provide :my_tutorial/Add "65e00b0ea70b5737f440c3d63afd8c59")


