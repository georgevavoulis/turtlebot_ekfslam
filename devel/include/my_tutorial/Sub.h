// Generated by gencpp from file my_tutorial/Sub.msg
// DO NOT EDIT!


#ifndef MY_TUTORIAL_MESSAGE_SUB_H
#define MY_TUTORIAL_MESSAGE_SUB_H

#include <ros/service_traits.h>


#include <my_tutorial/SubRequest.h>
#include <my_tutorial/SubResponse.h>


namespace my_tutorial
{

struct Sub
{

typedef SubRequest Request;
typedef SubResponse Response;
Request request;
Response response;

typedef Request RequestType;
typedef Response ResponseType;

}; // struct Sub
} // namespace my_tutorial


namespace ros
{
namespace service_traits
{


template<>
struct MD5Sum< ::my_tutorial::Sub > {
  static const char* value()
  {
    return "4d5a10e57b9a637e659d8a36ed8ee7e5";
  }

  static const char* value(const ::my_tutorial::Sub&) { return value(); }
};

template<>
struct DataType< ::my_tutorial::Sub > {
  static const char* value()
  {
    return "my_tutorial/Sub";
  }

  static const char* value(const ::my_tutorial::Sub&) { return value(); }
};


// service_traits::MD5Sum< ::my_tutorial::SubRequest> should match 
// service_traits::MD5Sum< ::my_tutorial::Sub > 
template<>
struct MD5Sum< ::my_tutorial::SubRequest>
{
  static const char* value()
  {
    return MD5Sum< ::my_tutorial::Sub >::value();
  }
  static const char* value(const ::my_tutorial::SubRequest&)
  {
    return value();
  }
};

// service_traits::DataType< ::my_tutorial::SubRequest> should match 
// service_traits::DataType< ::my_tutorial::Sub > 
template<>
struct DataType< ::my_tutorial::SubRequest>
{
  static const char* value()
  {
    return DataType< ::my_tutorial::Sub >::value();
  }
  static const char* value(const ::my_tutorial::SubRequest&)
  {
    return value();
  }
};

// service_traits::MD5Sum< ::my_tutorial::SubResponse> should match 
// service_traits::MD5Sum< ::my_tutorial::Sub > 
template<>
struct MD5Sum< ::my_tutorial::SubResponse>
{
  static const char* value()
  {
    return MD5Sum< ::my_tutorial::Sub >::value();
  }
  static const char* value(const ::my_tutorial::SubResponse&)
  {
    return value();
  }
};

// service_traits::DataType< ::my_tutorial::SubResponse> should match 
// service_traits::DataType< ::my_tutorial::Sub > 
template<>
struct DataType< ::my_tutorial::SubResponse>
{
  static const char* value()
  {
    return DataType< ::my_tutorial::Sub >::value();
  }
  static const char* value(const ::my_tutorial::SubResponse&)
  {
    return value();
  }
};

} // namespace service_traits
} // namespace ros

#endif // MY_TUTORIAL_MESSAGE_SUB_H
